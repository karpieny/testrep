

import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.FlatteningPathIterator;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;

class MyStroke implements Stroke {
    float step;
    float flatness = 1;
    Stroke stroke;

    public MyStroke(Stroke stroke, float step) {
        this.step = step;
        this.stroke = stroke;
    }

    public Shape createStrokedShape(Shape shape) {
        GeneralPath result = new GeneralPath();
        PathIterator it = new FlatteningPathIterator(shape.getPathIterator(null), flatness);
        float points[] = new float[6];
        float moveX = 0, moveY = 0;
        float lastX = 0, lastY = 0;
        float thisX = 0, thisY = 0;
        int type = 0;
        float next = 0;
        int phase = 0;

        while (!it.isDone()) {
            type = it.currentSegment(points);
            switch(type){
                case PathIterator.SEG_MOVETO:
                    moveX = lastX = points[0];
                    moveY = lastY = points[1];
                    result.moveTo( moveX, moveY );
                    next = step/2;
                    break;

                case PathIterator.SEG_CLOSE:
                    points[0] = moveX;
                    points[1] = moveY;

                case PathIterator.SEG_LINETO:
                    thisX = points[0];
                    thisY = points[1];
                    float dx = thisX - lastX;
                    float dy = thisY - lastY;
                    float distance = (float)Math.sqrt(dx * dx + dy * dy);
                    if (distance >= next) {
                        while (distance >= next) {
                            float x = lastX + next * dx / distance;
                            float y = lastY + next * dy / distance;
                            switch (phase % 2) {
                                case 0:
                                	result.lineTo(x , y);
                                    ++phase;
                                    break;
                                case 1:
                                    result.lineTo(x + step * dy / distance * 2, y - step * dx / distance * 2);
                                    result.lineTo(x, y);
                                    ++phase;
                                    break;
                            }
                            next += step;
                        }
                    }
                    next -= distance;
                    lastX = thisX;
                    lastY = thisY;
                    if (type == PathIterator.SEG_CLOSE)
                        result.closePath();
                    break;
            }
            it.next();
        }

        return stroke.createStrokedShape(result);
    }
}


import java.awt.BasicStroke;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import static java.awt.geom.PathIterator.SEG_LINETO;
import static java.awt.geom.PathIterator.SEG_MOVETO;
import static java.awt.geom.PathIterator.WIND_NON_ZERO;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;


public class Rose implements Shape {

    private int a;
    private int radius;
    private Shape strokedShape;

    public Rose(int a_, int radius_) {
        this.a = a_;
        this.radius = radius_;
        strokedShape = new BasicStroke().createStrokedShape(this);
    }

    @Override
    public Rectangle getBounds() {
        return strokedShape.getBounds();
    }

    @Override
    public Rectangle2D getBounds2D() {
        return strokedShape.getBounds2D();
    }

    @Override
    public boolean contains(double x_, double y_) {
        return strokedShape.contains(x_, y_);
    }

    @Override
    public boolean contains(Point2D p_) {
        return strokedShape.contains(p_);
    }

    @Override
    public boolean intersects(double x_, double y_, double w_, double h_) {
        return strokedShape.intersects(x_, y_, w_, h_);
    }

    @Override
    public boolean intersects(Rectangle2D r_) {
        return strokedShape.intersects(r_);
    }

    @Override
    public boolean contains(double x_, double y_, double w_, double h_) {
        return strokedShape.contains(x_, y_, w_, h_);
    }

    @Override
    public boolean contains(Rectangle2D r_) {
        return strokedShape.contains(r_);
    }

    @Override
    public PathIterator getPathIterator(AffineTransform at_) {
        return new RoseIterator(at_, radius / 2000.0);
    }

    @Override
    public PathIterator getPathIterator(AffineTransform at_, double flatness_) {
        return new RoseIterator(at_, flatness_);
    }

    private class RoseIterator implements PathIterator {
        AffineTransform transform;
        double flatness;
        double t = 0;
        boolean done = false;

        public RoseIterator(AffineTransform transform_, double flatness_) {
            this.transform = transform_;
            this.flatness = flatness_;
        }

        public int getWindingRule() {
            return WIND_NON_ZERO;
        }

        public boolean isDone() {
            return done;
        }

        public int currentSegment(float[] coords_) {
            coords_[1] = (float) (Math.sin(a * t)* radius);
           coords_[0] = (float) ((a * t) *radius);
//               coords_[0] = (float) (Math.cos(a * t) * Math.cos(t) * radius);
//           coords_[1] = (float) (Math.cos(a * t) * Math.sin(t) * radius);


            if (transform != null)
                transform.transform(coords_, 0, coords_, 0,1);

            if (t >= 2 * Math.PI)
                done = true;

            if (t == 0)
                return SEG_MOVETO;

            return SEG_LINETO;
        }

        public int currentSegment(double[] coords_) {
//           coords_[0] =  (Math.cos(a * t) * Math.cos(t) * radius);    
//           coords_[1] =  (Math.cos(a * t) * Math.sin(t) * radius);
            coords_[1] =  (Math.sin(a * t)  * radius);    
           coords_[0] = ((a * t) *radius );
           

            if (transform != null)
                transform.transform(coords_, 0, coords_, 0,1);

            if (t >= 2 * Math.PI)
                done = true;

            if (t == 0)
                return SEG_MOVETO;

            return SEG_LINETO;
        }

        public void next() {
            if (done)
                return;

            t += flatness;
        }
    }
}



import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Shape;

import javax.swing.JApplet;

public class RoseApplet extends JApplet {
	private static final long serialVersionUID = 1L;
	static final int CX = 600, CY = 600;
    Color fillColor;
    Color borderColor;
    int strokeStep;
    int roseRadius;
    Point centerCoords;

    public Color getHtmlColor(String strRGB_, Color def_) {
        // in form [#RRGGBB]
        if (strRGB_ != null && strRGB_.charAt(0)== '#') {
            try {
                return new Color(Integer.parseInt(strRGB_.substring(1), 16));
            } catch (NumberFormatException e) {
                return def_;
            }
        }
        return def_;
    }

    public int getInt( String strInt_, int def_){
        // in form [int]
        if (strInt_ != null) {
            try {
                return Integer.parseInt(strInt_);
            } catch (NumberFormatException e) {
                return def_;
            }
        }
        return def_;
    }

    public Point getCoords(String strCoords_, Point def_){
        // in form [int int]
        if (strCoords_ != null){
            try{
                String[] temp = strCoords_.split(" ");
                if (temp.length != 2)
                    throw new Exception();
                return new Point(Integer.parseInt(temp[0]), Integer.parseInt(temp[1]));
            }
            catch (Exception e){
                return def_;
            }
        }
        return def_;
    }
    
    public void init() {
        setSize(CX, CY);
        setLayout(null);
        fillColor = getHtmlColor(getParameter("FillColor"), new Color(107, 151, 250));
        borderColor = getHtmlColor(getParameter("BorderColor"), Color.ORANGE);
        strokeStep = getInt(getParameter("StrokeStep"), 6);
        centerCoords = getCoords(getParameter("CenterCoordinates"), new Point(CX / 2,CY / 2));
        roseRadius = getInt(getParameter("Radius"), 200);
    }

   
    public void paint(Graphics g_){
        Graphics2D g2 = (Graphics2D) g_;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        Shape rose = new Rose(2, roseRadius);
        g2.translate(centerCoords.x, centerCoords.y);
        g2.setStroke(new MyStroke(new BasicStroke(2), strokeStep));
        g2.setColor(borderColor);
        g2.draw(rose);
        g2.setColor(fillColor);
        g2.fill(rose);
    }
}
