//c++ mvs 2015 
//Карпиеня 
#include <iostream>
#include <conio.h>
#include <math.h>
#include <fstream>
using namespace std;
int main() {
	fstream fileIn;
	fileIn.open("input.txt", ios::in); 
	char* text=new char[10];
	fileIn >> text;

	char *next_token = NULL;
	char * pch = strtok_s(text, ";",&next_token);
	double ProbA = atof(pch);
	pch = strtok_s(NULL, ";", &next_token);
	double ProbB = atof(pch);
	double Result=0;

	double WinA = ProbA*(1 - ProbB);//победа в микроматче первой
	double WinB = ProbB*(1 - ProbA);//победа в микроматче второй
	double Draw = 1 - ProbA - ProbB;//ничья

	//суммируем вероятности выигрышных ситуаций для первой команды

	Result+= 5*ProbA*pow((1-ProbA),4)*pow ((1-ProbB),5)+                      //1:0
		     6*pow (ProbA,2)*pow((1-ProbA),2)*pow ((1-ProbB),4)+              //2:0 "мгновенная смерть" при выполнененых 4-х ударах каждой командой
		     4*pow (ProbA,2)*pow ((1-ProbA),3)*pow ((1-ProbB),4)+             //2:0 "мгновенная смерть" после 5-го удара командой А
		     50*pow (ProbA,2)*pow ((1-ProbA),3)*ProbB*pow ((1-ProbB),4)+      //2:1
		     pow (ProbA,3)*pow((1-ProbB),3)+                                  //3:0 "мгновенная смерть" после 3-х ударов обеими командами
		     3*pow(ProbA,3)*(1-ProbA)*pow((1-ProbB),3)+                       //3:0 "мгновенная смерть" после 4-го удара первой командой
		     12*pow(ProbA,3)*(1-ProbA)*ProbB*pow((1-ProbB),3)+                //3:1 "мгновенная смерть" после 4-х ударов обеими
		     24*pow(ProbA,3)*pow((1-ProbA),2)*ProbB*pow((1-ProbB),3)+         //3:1 "мгновенная смерть" после 5-го  удара первой командой
		     100*pow(ProbA,3)*pow((1-ProbA),2)*pow(ProbB,2)*pow ((1-ProbB),3)+//3:2
		     3*pow(ProbA,4)*ProbB*pow((1-ProbB),2)+                           //4:1 "мгновенная смерть"
		     24*pow (ProbA,4)*(1-ProbA)*pow(ProbB,2)*pow((1-ProbB),2)+        //4:2 "мгновенная смерть"
		     50*pow (ProbA,4)*(1-ProbA)*pow(ProbB,3)*pow((1-ProbB),2)+        //4:3
		     4*pow (ProbA,5)*pow(ProbB,3)*(1-ProbB)+                          //5:3 "мгновенная смерть"
		     5*pow (ProbA,5)*pow (ProbB,4)*(1-ProbB)+                         //5:4
	         WinA*pow(Draw, 5) / (WinA + WinB);                               /*сумма бесконечно убывающей геометрической прогрессии
	//с первым членом WinA*pow(Draw, 5) и знаменателем Draw. 
	//6:5, 7:6, 8:7, 9:8...*/

	fstream fileOut;
	fileOut.open("output.txt", ios::out);
	fileOut << Result;

	fileOut.close(); 
	fileIn.close();
}